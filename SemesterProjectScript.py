import pymongo
from thehive4py.api import *
from thehive4py.models import *
import requests
import json
import time
import sys
import configparser

#### IMPORT CONFIGURATION ####
config = configparser.ConfigParser()
config.read('config.ini')
try:
  IP = "http://"+config['TheHive']['thehive_ip']+":"+config['TheHive']['thehive_port']
  BEARER = config['TheHive']['thehive_api_key']
  MONGO_IP = config['MongoDB']['mongodb_ip']
  MONGO_PORT = config['MongoDB']['mongodb_port']
  CORTEX_ID = config['Cortex']['cortex_id']
  OBS_TYPE_AS_TAG = config['Misc']['observer_type_as_tag'] == "True"
except KeyError as e:
  print('ERROR: Missing something in configuration file')
  raise_with_traceback(e)
##############################

# Configure TheHive4py API with IP and authentication credentials
api = TheHiveApi(IP,BEARER)

DB_NAME = "TheHiveAutomationAddon"
CASE_TO_OBS_DB = "TheHiveCaseToObservables"
OBS_TO_ANAL_DB = "TheHiveObsToAnalyzer"

try:
  mongo_client = pymongo.MongoClient("mongodb://"+MONGO_IP+":"+MONGO_PORT)
except pymongo.errors.ServerSelectionTimeoutError as e:
  print("ERROR: connection to mongodb timed out, make sure that mongodb is on and the IP and port are correct")
  raise_with_traceback(e)

# Create a database
database = mongo_client[DB_NAME]
print(mongo_client.list_database_names())

dblist = mongo_client.list_database_names()
if DB_NAME not in dblist:
  print(DB_NAME+" database not found, creating")

#Create a collection
case_to_obs = database[CASE_TO_OBS_DB]
case_to_obs.create_index([("obsId",pymongo.DESCENDING)], unique=True)
obs_to_anal = database[OBS_TO_ANAL_DB]
obs_to_anal.create_index([("obsTag",pymongo.DESCENDING),("analId",pymongo.DESCENDING)],unique=True)
print(database.list_collection_names())

collist = database.list_collection_names()
if CASE_TO_OBS_DB not in collist:
  print(CASE_TO_OBS_DB+" collection not found, creating")
if OBS_TO_ANAL_DB not in collist:
  print(OBS_TO_ANAL_DB+" collection not found, creating")

def insert_CASE_TO_OBS_DB(caseId, obsId, flag):
  try:
    case_to_obs.insert_one({"caseId": caseId, "obsId": obsId, "flag": flag})
  except pymongo.errors.DuplicateKeyError:
    print("Observable with id "+obsId+" already in database")

def insert_OBS_TO_ANAL_DB(obsTag, analId):
  try:
    obs_to_anal.insert_one({"obsTag": obsTag, "analId": analId})
  except pymongo.errors.DuplicateKeyError:
    print(obsTag + " + " + analId + " pairing already in database")

def mark_as_processed(obsId):
  case_to_obs.find_one_and_update({"obsId": obsId}, {'$set': {"flag": 0}})

def mark_as_unprocessed(obsId):
  case_to_obs.find_one_and_update({"obsId": obsId}, {'$set': {"flag": 1}})

def findObservables(caseId):
  global case_to_obs
  mydoc = case_to_obs.find({"caseId": caseId, "flag": 1})
  return mydoc

def findCase(observableId):
  global case_to_obs
  mydoc = case_to_obs.find({"obsId": observableId})
  return mydoc

def findAnalyzer(obsTag):
  global obs_to_anal
  mydoc = obs_to_anal.find({"obsTag": obsTag})
  return mydoc

def populate():
  global IP
  global BEARER

  query = {"_and": [{"_field": "status", "_value": "Open"}]}
  r = api.find_cases(query=query,range='all')
  for case in r.json():
    caseId = case.get('_id')
    req = api.get_case_observables(caseId)
    for obsv in req.json():
      # Consider all already present observables as processed
      insert_CASE_TO_OBS_DB(caseId,obsv.get('_id'),0)

def launch(caseId,obsId):
  global BEARER
  global IP
  global IP_CORTEX
  mark_as_processed(obsId)
  obsId = str(obsId)
  url = IP + "/api/case/artifact/" + obsId
  hdr = {'Authorization': 'Bearer ' + BEARER}
  r = requests.get(url, headers=hdr)
  obsJson = json.loads(r.text)
  obsTags = obsJson.get('tags')
  for obsTag in obsTags:
    analyzers = findAnalyzer(obsTag)
    no_analyzer = True
    for analy in analyzers:
      no_analyzer = False
      analId = str(analy.get('analId'))
      r = api.run_analyzer(CORTEX_ID,obsId,analId)
      json_request = r.json()
      jobId = json_request.get('id')
      status = json_request.get('status')
      report = None

      reportDone = False
      while (not reportDone) and status != "Failure":
        time.sleep(5)
        url = IP + "/api/connector/cortex/job/"+jobId
        hdr = {'Authorization': 'Bearer ' + BEARER}
        r = requests.get(url, headers=hdr)
        json_request = r.json()
        if json_request.get('report') != None and report == json_request.get('report'):
          # If we receive the same report twice in a row, we know that we are not waiting for more information
          reportDone = True
        report = json_request.get('report')
        status = json_request.get('status')

      if status != "Failure" and report.get('success'):
        new_observables = report.get('artifacts')
        print("adding "+str(len(new_observables))+" observables")
        for new_obs in new_observables:
          if OBS_TYPE_AS_TAG:
            new_tags = new_obs.get('tags') + [new_obs.get('dataType')]
          else:
            new_tags = new_obs.get('tags')
          case_observable = CaseObservable(dataType=new_obs.get('dataType'),
                                           data=new_obs.get('data'),
                                           tlp=new_obs.get('tlp'),
                                           ioc=new_obs.get('ioc'),
                                           sighted=new_obs.get('sighted'),
                                           tags=new_tags,
                                           message=new_obs.get('message')
                                           )
          api.create_case_observable(caseId, case_observable)
      else:
        print("ERROR: cortex analyzer failed")
    if no_analyzer:
      print("INFO: no analyzer found for "+ obsTag +" tag")

if __name__ == "__main__":

  if sys.argv[1] == "DROP":
    case_to_obs.delete_many({})
    obs_to_anal.delete_many({})
  elif sys.argv[1] == "LIST":
    mydoc = case_to_obs.find({})
    print("Listing case to observable content:")
    for x in mydoc:
      print(x)
    mydoc = obs_to_anal.find({})
    print("Listing observable to analyzer content:")
    for x in mydoc:
      print(x)
  elif sys.argv[1] == "POPULATE":
    populate()
  elif sys.argv[1] == "TEST_LAUNCH":
    caseId = "122884336"
    obsId = "245776584"
    mark_as_unprocessed(obsId)
    launch(caseId,obsId)
  elif sys.argv[1] == "ADD_ANALYZER":
    obsTag = sys.argv[2]
    analId = sys.argv[3]
    insert_OBS_TO_ANAL_DB(obsTag,analId)

  elif sys.argv[1] == "REMOVE_ANALYZER":
    obsTag = sys.argv[2]
    analId = sys.argv[3]
    print("Deleted: "+str(obs_to_anal.find_one({"obsTag":obsTag,"analId":analId})))
    obs_to_anal.delete_one({"obsTag":obsTag,"analId":analId})

  elif sys.argv[1] == "LIST_ANALYZERS":
    url = IP + "/api/connector/cortex/analyzer"
    hdr = {'Authorization': 'Bearer ' + BEARER}
    r = requests.get(url, headers=hdr)
    json_request = json.loads(r.text)
    for analyzer in json_request:
      print(analyzer.get('name')+": "+analyzer.get('id'))

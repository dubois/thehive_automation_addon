import socket
import SemesterProjectScript
import traceback
from threading import Thread

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to the port
server_address = ('localhost', 14832)
print('starting up on port ' + str(server_address))
sock.bind(server_address)
# Listen for incoming connections
sock.listen(1)

class LaunchThread(Thread):

    def __init__(self,caseId,obsId):
        Thread.__init__(self)
        self.caseId = caseId
        self.obsId = obsId

    def run(self):
        SemesterProjectScript.launch(caseId,obsId)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from '+ str(client_address) + ' opened')
        while True:
                data = connection.recv(1024)
                strdata = str(data, 'utf-8')
                if not data or data == b'\n':
                    print("connection from " + str(client_address) + " closed")
                    connection.close()
                    break
                else:
                    caseId, obsId = strdata.split(',')
                    print('received caseId = ' + caseId + ', obsId = ' + obsId)
                    SemesterProjectScript.insert_CASE_TO_OBS_DB(caseId, obsId, 1)
                    thread = LaunchThread(caseId,obsId)
                    thread.start()

    except Exception:
        traceback.print_exc()

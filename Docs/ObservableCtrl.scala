package org.thp.thehive.controllers.v0

import scala.util.Success

import play.api.Logger
import play.api.libs.json.JsObject
import play.api.mvc.{Action, AnyContent, Results}

import javax.inject.{Inject, Singleton}
import org.thp.scalligraph._
import org.thp.scalligraph.controllers._
import org.thp.scalligraph.models.Database
import org.thp.scalligraph.query.{ParamQuery, PropertyUpdater, PublicProperty, Query}
import org.thp.scalligraph.steps.PagedResult
import org.thp.scalligraph.steps.StepsOps._
import org.thp.thehive.controllers.v0.Conversion._
import org.thp.thehive.dto.v0.InputObservable
import org.thp.thehive.models._
import org.thp.thehive.services._
import play.api.Logger
import java.io.FileWriter
import java.io.BufferedReader
import java.io.FileReader
import sys.process._
import scala.language.postfixOps
import java.net._
import java.io._
import scala.io._

@Singleton
class ObservableCtrl @Inject() (
    entrypoint: Entrypoint,
    db: Database,
    properties: Properties,
    observableSrv: ObservableSrv,
    observableTypeSrv: ObservableTypeSrv,
    caseSrv: CaseSrv,
    organisationSrv: OrganisationSrv
) extends QueryableCtrl
    with ObservableRenderer {

  lazy val logger: Logger                                   = Logger(getClass)
  override val entityName: String                           = "observable"
  override val publicProperties: List[PublicProperty[_, _]] = properties.observable ::: metaProperties[ObservableSteps]
  override val initialQuery: Query =
    Query.init[ObservableSteps]("listObservable", (graph, authContext) => organisationSrv.get(authContext.organisation)(graph).shares.observables)
  override val getQuery: ParamQuery[IdOrName] = Query.initWithParam[IdOrName, ObservableSteps](
    "getObservable",
    FieldsParser[IdOrName],
    (param, graph, authContext) => observableSrv.get(param.idOrName)(graph).visible(authContext)
  )
  override val pageQuery: ParamQuery[OutputParam] = Query.withParam[OutputParam, ObservableSteps, PagedResult[(RichObservable, JsObject)]](
    "page",
    FieldsParser[OutputParam], {
      case (OutputParam(from, to, withStats, _), observableSteps, authContext) =>
        observableSteps
          .richPage(from, to, withTotal = true) {
            case o if withStats =>
              o.richObservableWithCustomRenderer(observableStatsRenderer(authContext))
            case o =>
              o.richObservable.map(_ -> JsObject.empty)
          }
    }
  )
  override val outputQuery: Query = Query.output[(RichObservable, JsObject)]()
  override val extraQueries: Seq[ParamQuery[_]] = Seq(
    Query[ObservableSteps, List[RichObservable]]("toList", (observableSteps, _) => observableSteps.richObservable.toList)
  )

  def create(caseId: String): Action[AnyContent] =
    entrypoint("create artifact")
      .extract("artifact", FieldsParser[InputObservable])
      .authTransaction(db) { implicit request => implicit graph =>
        val inputObservable: InputObservable = request.body("artifact")
        lazy val logger: Logger = Logger("ObservableCtrl0")
        logger.info("ObservableCtrl v0")
     
        val observables = {
          for {
            case0 <- caseSrv
              .get(caseId)
              .can(Permissions.manageObservable)
              .getOrFail()
            observableType <- observableTypeSrv.getOrFail(inputObservable.dataType)
            observablesWithData <- inputObservable
              .data
              .toTry(d => observableSrv.create(inputObservable.toObservable, observableType, d, inputObservable.tags, Nil))
            observableWithAttachment <- inputObservable
              .attachment
              .map(a => observableSrv.create(inputObservable.toObservable, observableType, a, inputObservable.tags, Nil))
              .flip
            createdObservables <- (observablesWithData ++ observableWithAttachment).toTry { richObservables =>
              caseSrv
                .addObservable(case0, richObservables)
                .map(_ => richObservables)
            }
          } yield createdObservables
        }      
        
        val s = sendToAutomationAddon(caseId,observables)

        for (observable <- observables)
        yield Results.Created(observable.toJson)
      }
  
  def sendToAutomationAddon(caseId:String,observables:scala.util.Try[Seq[org.thp.thehive.models.RichObservable]] ): scala.util.Try[Int] = {
        scala.util.Try({    
            val s = new Socket(InetAddress.getByName("localhost"), 14832)
            val out = new PrintStream(s.getOutputStream())
    			  observables.foreach(observable => {
                    val json = observable.toJson
                    val id = json(0)("_id").toString()
                    out.println(caseId+","+id.substring(1, id.toString().length()-1))
                    out.flush()
                  })
            val c = s.close()
            1
        })
  }

  def get(observableId: String): Action[AnyContent] =
    entrypoint("get observable")
      .authRoTransaction(db) { implicit request => implicit graph =>
        observableSrv
          .getByIds(observableId)
          .visible
          .richObservable
          .getOrFail()
          .map { observable =>
            Results.Ok(observable.toJson)
          }
      }

  def update(observableId: String): Action[AnyContent] =
    entrypoint("update observable")
      .extract("observable", FieldsParser.update("observable", publicProperties))
      .authTransaction(db) { implicit request => implicit graph =>
        val propertyUpdaters: Seq[PropertyUpdater] = request.body("observable")
        observableSrv
          .update(
            _.getByIds(observableId).can(Permissions.manageObservable),
            propertyUpdaters
          )
          .map(_ => Results.NoContent)
      }

  def findSimilar(obsId: String): Action[AnyContent] =
    entrypoint("find similar")
      .authRoTransaction(db) { implicit request => implicit graph =>
        val observables = observableSrv
          .getByIds(obsId)
          .visible
          .similar
          .visible
          .richObservableWithCustomRenderer(observableLinkRenderer)
          .toList

        Success(Results.Ok(observables.toJson))
      }

  def bulkUpdate: Action[AnyContent] =
    entrypoint("bulk update")
      .extract("input", FieldsParser.update("observable", publicProperties))
      .extract("ids", FieldsParser.seq[String].on("ids"))
      .authTransaction(db) { implicit request => implicit graph =>
        val properties: Seq[PropertyUpdater] = request.body("input")
        val ids: Seq[String]                 = request.body("ids")
        ids
          .toTry { id =>
            observableSrv
              .update(_.getByIds(id).can(Permissions.manageObservable), properties)
          }
          .map(_ => Results.NoContent)
      }

  def delete(obsId: String): Action[AnyContent] =
    entrypoint("delete")
      .authTransaction(db) { implicit request => implicit graph =>
        for {
          observable <- observableSrv
            .getByIds(obsId)
            .can(Permissions.manageObservable)
            .getOrFail()
          _ <- observableSrv.cascadeRemove(observable)
        } yield Results.NoContent
      }
}

